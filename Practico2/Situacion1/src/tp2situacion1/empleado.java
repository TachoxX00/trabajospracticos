/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tp2situacion1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dante
 */
public class Empleado {
    
    private String nombre;
    private String apellido;
    private double costoPorHora;
    private List<Turno> turnos;
    
    public Empleado(String nombre, String apellido, double costoPorHora){
        this.nombre = nombre;
        this.apellido = apellido;
        this.costoPorHora = costoPorHora;
        this.turnos = new ArrayList<>();
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setCostoPorHora(double costoPorHora) {
        this.costoPorHora = costoPorHora;
    }

    public void setTurnos(List<Turno> turnos) {
        this.turnos = turnos;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public double getCostoPorHora() {
        return costoPorHora;
    }

    public List<Turno> getTurnos() {
        return turnos;
    }
    
    public void agregarTurno(){
    }
    
    public long calcularSueldoPorTurno(Turno turno){
        long horasTrabajadas = turno.calcularHorasTrabajadas();
        return (long) (horasTrabajadas * costoPorHora);
    }
    
    public double calcularSueldo(){
        double sueldoTotal = 0.0;
        for(Turno turno : turnos){
            sueldoTotal += calcularSueldoPorTurno(turno);
        }
        return sueldoTotal;
    }
    
    public void mostrar(){
        System.out.println("Nombre: " + nombre);
        System.out.println("Apellido: " + apellido);
        System.out.println("Costo de hora trabajada: " + costoPorHora);
        System.out.println("----- Turnos realizados -----");
        for (Turno turno : turnos){
            System.out.println("Fecha y hora Ingreso: " +turno.getFechaIngreso);
            System.out.println("Fecha y hora Egreso: " +turno.getFechaEgreso);
        }
    } 
}
