/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tp2situacion1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dante
 */
public class Turno {
    
    private LocalDate fechaIngreso;
    private LocalDate fechaEgreso;
    String getFechaIngreso;
    String getFechaEgreso;
    
    public Turno(LocalDate fechaIngreso, LocalDate fechaEgreso){
        this.fechaIngreso = fechaIngreso;
        this.fechaEgreso = fechaEgreso;
    }
    
    public Turno(){
        
    }

    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public void setFechaEgreso(LocalDate fechaEgreso) {
        this.fechaEgreso = fechaEgreso;
    }
    
    

    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    public LocalDate getFechaEgreso() {
        return fechaEgreso;
    }
    
    public long calcularHorasTrabajadas(){
        return fechaIngreso.until(fechaEgreso, java.time.temporal.ChronoUnit.HOURS);
    }   

}
