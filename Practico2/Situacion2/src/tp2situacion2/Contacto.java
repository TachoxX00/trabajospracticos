/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tp2situacion2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dante
 */
public class Contacto {
    
    private String nombre;
    private String correoElectronico;
    private String numeroTelefono;

    public Contacto(String nombre, String correoElectronico, String numeroTelefono) {
        this.nombre = nombre;
        this.correoElectronico = correoElectronico;
        this.numeroTelefono = numeroTelefono;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }


    @Override
    public String toString() {
        return "Nombre: " + nombre + ", Correo: " + correoElectronico + ", Teléfono: " + numeroTelefono;
    }
    
}