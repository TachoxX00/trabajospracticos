/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tp2situacion2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dante
 */
public class AgendaContacto{
    private List<Contacto> contactos;
    private int capacidadMaxima;

    public AgendaContacto(int capacidadMaxima) {
        this.capacidadMaxima = capacidadMaxima;
        this.contactos = new ArrayList<>();
    }

    public void agregarContacto(Contacto contacto) {
        if (contactos.size() < capacidadMaxima) {
            contactos.add(contacto);
            System.out.println("CONTACTO AGREGADO");
        } else {
            System.out.println("AGENDA LLENA - NO SE PUDO AGREGAR EL CONTACTO.");
        }
    }

    public Contacto buscarContacto(String nombre) {
        for (Contacto contacto : contactos) {
            if (contacto.getNombre().equalsIgnoreCase(nombre)) {
                return contacto;
            }
        }
        return null; // Contacto no encontrado
    }

    public void modificarContacto(String nombre, Contacto nuevoContacto) {
        Contacto contactoExistente = buscarContacto(nombre);
        if (contactoExistente != null) {
            int indice = contactos.indexOf(contactoExistente);
            contactos.set(indice, nuevoContacto);
            System.out.println("SE MODIFICO EL CONTACTO.");
        } else {
            System.out.println("CONTACTO NO ENCONTRADO - NO SE PUDO MODIFICAR");
        }
    }

    public void eliminarContacto(String nombre) {
        Contacto contactoExistente = buscarContacto(nombre);
        if (contactoExistente != null) {
            contactos.remove(contactoExistente);
            System.out.println("CONTACTO ELIMINADO");
        } else {
            System.out.println("CONTACTO NO ENCONTRADO - NO SE PUDO ELIMINAR");
        }
    }

    public void listarContactos() {
        for (Contacto contacto : contactos) {
            System.out.println(contacto.toString());
        }
    }
}