/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package tp2situacion2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dante
 */
public class TP2Situacion2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        AgendaContacto agenda = new AgendaContacto(150);

        // Agregar contactos
        agenda.agregarContacto(new Contacto("Fabrizio Endrizzi", "dantefabrizioendrizzileiva@gmail.com", "3834002967"));
        agenda.agregarContacto(new Contacto("Lionel Messi", "lionelmessi@gmail.com", "01164739382"));

        // Buscar contacto
        Contacto contacto = agenda.buscarContacto("Fabrizio Endrizzi");
        if (contacto != null) {
            System.out.println("CONTACTO: " + contacto.toString());
        } else {
            System.out.println("CONTACTO NO ENCONTRADO");
        }

        // Modificar contacto
        Contacto nuevoContacto = new Contacto("Fabrizio Endrizzi", "dantefabrizioendrizzileiva@gmail.com", "3834002967");
        agenda.modificarContacto("Fabrizio Endrizzi", nuevoContacto);

        // Listar contactos
        agenda.listarContactos();
    }
    
}
